import json
import re
# import numpy as np

#Data base for Michael and Greg with reference to the Recipe Data base
#The keys within one of the recipe dictionaries are
# directions, fat, date, categories, calories, desc, protein, rating, title,
# ingredients, sodium

#DIFFERENT FUNCTIONS WE WILL IMPLEMENT FOR THE API
# - Let them put in different ingredients and we choose a recipe that has the most
# in them with the highest rating. Difficult part with this is that ingredients
# are strings with size amounts and descriptions. So we might need to parse each of these
# and just have individual words that they could match.
# - Let them put in a category and return highest rated meal with that category
# - Input a recipe into the database
# - Given thresholds of protein, fat, and sodium, return highest rated meal
# - Finding easiest meal to make that is highest rated (done by taking number
# of ingredients and finding length of directions)
# - Given a title, give the directions on how to make it
# - Allow rating of a meal from user Input
# - Add a custom review for a meal or helpful tips for the next person

#DATABASE FORMAT
#-Each recipe will become a class or a dictionary with different attributes
#-Each one will be given an id number as to reference them. So when reading in
#we will need to construct either one large dictionary or a dictionary that reference
#classes
#-If we are adding a whole new section, then put at the end of the queue

#NOTES ON database
#-Some of the elements in the list are blank. Element 1076 has nothing in it
# so that will need to be taken into account going through this
#-Ingredients aren't different elements, so we will need to strip each ingredient to be
#individual elements. Not a great way to do this but only way that we would be able to match
#apples to apples

class recipe:
	#def __init__(self):
	#	self.categories = []
	#	self.rating = 0
	#	self.directions = ''
	#	self.title = ''
	#	self.ingredients = []
	#	self.calories = 0

	def __init__(self, data):						#initializer for a recipe object from a dictionary input
		if data != []:
			self.categories = data['categories']
			self.rating = data['rating']
			self.directions = data['directions']
			self.title = data['title']
			self.ingredients = data['ingredients']
			self.calories = data['calories']
			self.sodium = data['sodium']
			self.protein = data['protein']
			self.fat = data['fat']
			total = 0
			for ingredient in self.directions:
				total += len(ingredient)
			if total < 539:
				self.difficulty = 'easy'
			elif total < 940:
				self.difficulty = 'medium'
			else:
				self.difficulty = 'hard'
			self.rec_id = 0
		else:									#if data is empty, make base values that will be set for an addition of a recipe
			self.categories = []
			self.rating = 0
			self.directions = ''
			self.title = ''
			self.ingredients = []
			self.calories = 0
			self.rec_id = 0
      self.difficulty = ''

class _recipe_database:
	def __init__(self):
		self.recipes = {}

	def load_recipes(self, filename):
		#hard_list = []
		data = open(filename, "r")
		data = json.load(data)
		void = [1077, 1136, 1908, 5147, 5425, 5559, 7608, 7769, 7882, 8178, 9591, 10086, 11225, 13207, 13945, 14685, 16211, 16904, 19548]  #ignore these as they are corrupt values in the data file
		rec_id = 1				#initial id from which the first recipe will be referenced
		for entry in data:
			#print rec_id

			if rec_id not in void:
				rec = recipe(entry)
				rec.rec_id = rec_id  			#assighn unique id
				# hard_list.append(rec.difficulty)
			self.recipes[rec_id] = rec
			rec_id +=1
		# print(np.percentile(hard_list, 33), 'easy')
		# print(np.percentile(hard_list, 66), 'medium')
		# print(np.percentile(hard_list, 100), 'hard')

	def get_recipe(self, rec_id):
		if rec_id in self.recipes.keys():
			return self.recipes[rec_id]
		else:
			return None

	def get_by_category(self, category):
		tempList = []
		for entry in self.recipes.keys():
			#print self.recipes[entry].categories
			if category.lower() in [x.lower() for x in self.recipes[entry].categories]:
				tempList.append(self.recipes[entry])

		return tempList

	def add_recipe(self, title, category, directions, rating, ingredients, calories):
		rec = recipe([])
		rec.categories = category
		rec.title = title
		rec.directions = directions
    total = 0
    for ingredient in rec.directions:
      total += len(ingredient)
    if total < 539:
      rec.difficulty = 'easy'
    elif total < 940:
      rec.difficulty = 'medium'
    else:
      rec.difficulty = 'hard'

    rec.rating = rating
		rec.ingredients = ingredients
		rec.calories = calories
		temp_max = len(self.recipes)			#retrieve size of list and add the recipe at the end
		rec.rec_id = temp_max +1
		self.recipes[rec.rec_id] = rec

	def get_highest_rated_by_category(self, category):
		temp_list = self.get_by_category(category)		#get all recipes of that category
		temp_max = 0
		temp_id = 0
		for rec in temp_list:					#index through while keeping a running max and reference to the recipe
			if rec.rating > temp_max:
				temp_max = rec.rating
				temp_id = rec.rec_id
		return self.recipes[temp_id]

	def get_most_ingredients(self, data):

		temp_max = 0
		temp_id = 0
		for i in self.recipes.keys():
			temp_sum = 0
			for j in data:
				j = j.lower()
				for k in self.recipes[i].ingredients:		
					stripped_ingred = []
					temp_ingred = k.split(' ')				#split ingredients string into individual words
					for ing in temp_ingred:
						ing = re.sub('[!@#$\,\(\)]', '', ing).lower()		#need to remove punction from words and standardize the case
						if ing == j:
							# print(temp_sum)
							# print(j)
							# print(self.recipes[i].title)
							temp_sum += 1

			if temp_sum > temp_max:				#keep running max
				temp_max = temp_sum
				temp_id = self.recipes[i].rec_id

		if temp_id == 0:
			return self.recipes[1]
		return self.recipes[temp_id]

	def get_difficulty_with_category(self, category, level):
		recipe_list = self.get_by_category(category)
		accurate_recipe = 'None with desired level'
		accurate_rating = 0
		for recipe in recipe_list:
			if recipe.difficulty == level:
				if recipe.rating > accurate_rating:
					accurate_recipe = recipe.title
					accurate_rating = recipe.rating

		return accurate_recipe

	def get_by_difficulty(self, level):
		tempList = []
		for entry in self.recipes.keys():
			#print self.recipes[entry].categories
      if level == self.recipes[entry].difficulty:
				tempList.append(self.recipes[entry])

		return tempList

	def get_best_desired_difficulty(self, level):
		list_difficulty = self.get_by_difficulty(level)
		best = list_difficulty[0]
		for element in list_difficulty:
			if element.rating > best.rating:
				best = element

		return best

	def get_healthiest_by_category(self, category):
		recipe_list = self.get_by_category(category)
		best = recipe_list[0]
		health_scale = 0.0
		for element in recipe_list:
			if element.protein == None or element.fat == None or element.sodium == None:
				continue
			else:
				healthy = (element.protein)/(element.fat + element.sodium)
				if healthy > health_scale:
					health_scale = healthy
					best = element

		return best
	
	def delete_recipe(self, id_num):
		self.recipes[id_num] = {}

	def delete_all_recipes(self):
		self.recipes = dict()

if __name__ == '__main__':
	test = _recipe_database()
	test.load_recipes('data/full_format_recipes.json')
	temp = test.get_by_category("Chicken")

	# test.add_recipe("Test", [], "test", 2, ["test", "test", "test"], 100)
	sand = test.get_highest_rated_by_category("Sandwich")
	ingred = ["zzzzzzz", "test"]
	temp = test.get_most_ingredients(ingred)
	temp = test.get_difficulty_with_category("Chicken", "easy")
	# temp = test.get_best_desired_difficulty("easy")
