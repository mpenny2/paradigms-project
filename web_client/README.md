The webservice supports many different request types to various URLs on port 51060.  I won't focus too much 
on the specifications of each request and what is included in the body and returned in this document
because that is covered extensively in our JSON specification document.  From a broad level, the recipe
URL can be considered the main URL for interaction purposes.  The reset extension on the port will load 
all of the recipes again from the database.  The user can then get all of the recipes, get specific recipes, 
add a recipe,  get the recipe with the most ingredient matches passed as arguments, get the highest rated
recipe by category, get the healthiest recipe by category, and get a meal which matches a certain difficulty level
per a given category.  The server also supports the deletion of specific elements as well as the whole database.
Once again, the specifics of each call are included in the JSON specifications document.

