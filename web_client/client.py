#Gregory Esrig and Michael Penny
#main file to run the starter in
import cherrypy
import re
import json
from _recipe_database import _recipe_database
from reset_controller import ResetController
from recipe_controller import RecipeController
from difficulty_controller import DifficultyController
from category_controller import CategoryController

class _options_controller(object):
	def OPTIONS(self, *args, **kargs):
		return ""


def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "*"


def start_service():
	options_controller = _options_controller()

	recipe_database = _recipe_database()
	ResetCont = ResetController(recipe_database)
	RecipeCont = RecipeController(recipe_database)
	DifficultyCont = DifficultyController(recipe_database)
	CategoryCont = CategoryController(recipe_database)
	dispatcher = cherrypy.dispatch.RoutesDispatcher()




	dispatcher.connect('rdb_reset_option', route = '/reset/', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))
	dispatcher.connect('rdb_get_option', route = '/recipes/', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))
	dispatcher.connect('rdb_get_key_option', route = '/recipes/:recipe_id', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))
	dispatcher.connect('rdb_recipe_post_option', route = '/recipes/', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))
	dispatcher.connect('rdb_recipe_delete_option', route = '/recipes/', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))
	dispatcher.connect('rdb_recipe_delete_key_option', route = '/recipes/:recipe_id', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))
	dispatcher.connect('rdb_difficulty_get_option', route = '/difficulty/:level', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))
	dispatcher.connect('rdb_category_get_option', route = '/categories/', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))
	dispatcher.connect('rdb_category_post', route = '/categories/:category', controller = options_controller, action = 'OPTIONS', conditions = dict(method = ['OPTIONS']))



	dispatcher.connect('rdb_reset', '/reset/', controller = ResetCont, action = 'PUT', conditions = dict(method = ['PUT']))
	dispatcher.connect('rdb_get', '/recipes/' , controller = RecipeCont, action = 'GET', conditions = dict(method = ['GET']))
	dispatcher.connect('rdb_get_key', '/recipes/:recipe_id', controller = RecipeCont, action = 'GET_KEY', conditions = dict(method = ['GET']))
	dispatcher.connect('rdb_recipe_post', '/recipes/' , controller = RecipeCont, action = 'POST', conditions = dict(method = ['POST']))
	dispatcher.connect('rdb_recipe_delete', '/recipes/', controller = RecipeCont, action = 'DELETE', conditions = dict(method = ['DELETE']))
	dispatcher.connect('rdb_recipe_delete_key', '/recipes/:recipe_id', controller = RecipeCont, action = 'DELETE_KEY', conditions = dict(method = ['DELETE']))
	dispatcher.connect('rdb_difficulty_get', '/difficulty/:level', controller = DifficultyCont, action = 'GET', conditions = dict(method = ['GET']))
	dispatcher.connect('rdb_category_get', '/categories/', controller = CategoryCont, action = 'GET', conditions = dict(method = ['GET']))
	dispatcher.connect('rdb_category_get', '/categories/:category', controller = CategoryCont, action = 'GET_CATEGORY', conditions = dict(method = ['GET']))
	dispatcher.connect('rdb_category_post', '/categories/:category', controller = CategoryCont, action = 'POST', conditions = dict(method = ['POST']))



	conf = {
			'global' : {'server.socket_host' : 'student04.cse.nd.edu',
						'server.socket_port' : 51028
						},
					'/': {
						'request.dispatch' : dispatcher,
						'tools.CORS.on' : True
						}
			}
	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config = conf)
	cherrypy.quickstart(app)


if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
	start_service()
