from _recipe_database import _recipe_database
import json
import re
import cherrypy

class RecipeController(object):
	def __init__(self, rdb = None):
		if rdb == None:
			self.recipe_db = _recipe_database()
		else:
			self.recipe_db = rdb


	#gets all recipes in database and returns a list of their titles
	def GET(self):
		output = {'result' : 'success'}
		temp_list = []
		try:
			for key in self.recipe_db.recipes.keys():
				temp_list.append(self.recipe_db.recipes[key].title)

			output['titles'] = temp_list
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		return json.dumps(output)

	#gets a specific recipe and returns all of its attributes
	def GET_KEY(self, recipe_id):
		output = {'result' : 'success'}
		recipe_id = int(recipe_id)
		try:
			output['title'] = self.recipe_db.recipes[recipe_id].title
			output['rating'] = self.recipe_db.recipes[recipe_id].rating
			output['directions'] = self.recipe_db.recipes[recipe_id].directions
			output['categories'] = self.recipe_db.recipes[recipe_id].categories
			output['ingredients'] = self.recipe_db.recipes[recipe_id].ingredients
			output['calories'] = self.recipe_db.recipes[recipe_id].calories
			output['protein'] = self.recipe_db.recipes[recipe_id].protein
			output['fat'] = self.recipe_db.recipes[recipe_id].fat
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		return json.dumps(output)

	#1) Adds a recipe by taking the attributes of the request is all of them are included
	#2) Can also return the recipe matching the most ingredients if that is what is in teh body
	def POST(self):
		output = {'result' : 'success'}
		the_body = cherrypy.request.body.read(int(cherrypy.request.headers['Content-Length']))
		try:
			the_body = json.loads(the_body)
			title = the_body['title']
			category = the_body['category']
			directions = the_body['directions']
			rating = the_body['rating']
			ingredients = the_body['ingredients']
			calories = the_body['calories']
			protein = the_body['protein']
			fat = the_body['fat']
			sodium = the_body['sodium']
			rec_id = self.recipe_db.add_recipe(title, category, directions, rating, ingredients, calories, protein, fat, sodium)
			output["rec_id"] = rec_id

		except KeyError as ex:
			try:
				ingredient_list = the_body['ingredients']
				best = self.recipe_db.get_most_ingredients(ingredient_list)
				output['title'] = best.title
				output['rec_id'] = best.rec_id
				output['rating'] = best.rating
			except KeyError as ey:
				output['result'] = 'error'
				output['message'] = 'key not found'

		return json.dumps(output)

	#deletes all recipes
	def DELETE(self):
		output = {'result' : 'success'}
		try:
			self.recipe_db.delete_all_recipes()
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'

		return json.dumps(output)


	#deletes specific recipe
	def DELETE_KEY(self, recipe_id):
		output = {'result' : 'success'}
		try:
			self.recipe_db.delete_recipe(int(recipe_id))
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'

		return json.dumps(output)

	def GET_LEVEL(self, level):
		output = {'result' : 'success'}
		try:
			best = self.recipe_db.get_best_desired_difficulty(level)
			output['title'] = best.title
			output['rec_id'] = best.rec_id
			output['rating'] = best.rating
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		return json.dumps(output)
