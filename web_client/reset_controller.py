from _recipe_database import _recipe_database
import json
import re
#Need PUT and PUT_KEY
class ResetController(object):
	def __init__(self, rdb = None):
		if rdb == None:
			self.recipe_db = _recipe_database()
		else:
			self.recipe_db = rdb

	def PUT(self):
		output = {'result' : 'success'}
		self.recipe_db.delete_all_recipes()
		self.recipe_db.load_recipes('data/full_format_recipes.json')
		return json.dumps(output)

	# def PUT_KEY(self,movie_id):
	# 	#print("\n\n\n\nWE ARE IN THE KEY FUNCTION\n\n\n\n")
	# 	output = {'result' : 'success'}
	# 	temp_dict = {}
	# 	movie_data = open('ml-1m/movies.dat', "r")
	# 	for des in movie_data:
	# 		temp = des.split("::")
	# 		if movie_id == int(temp[0]):
	# 			temp[2] = temp[2].rstrip()
	# 			self.movie_db.movies[int(temp[0])] = temp[1:]
	# 			break
	# 	return json.dumps(output)
