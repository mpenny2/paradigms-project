from _recipe_database import _recipe_database
import json
import re
import cherrypy

class CategoryController(object):
	def __init__(self, rdb = None):
		if rdb == None:	
			self.recipe_db = _recipe_database()		
		else:
			self.recipe_db = rdb


	def GET(self):
		output = {'result' : 'success'}
		temp_list = []
		try:
			for key in self.recipe_db.recipes.keys():			#gets all items associated with a category
				for category in self.recipe_db.recipes[key].categories:
					if category.lower() not in temp_list:
						temp_list.append(category.lower())


			output['Categories'] = temp_list
		except KeyError as ex:					#if failure, return error
			output['result'] = 'error'
			output['message'] = 'key not found'
		return json.dumps(output)

	def GET_CATEGORY(self, category):
		output = {'result' : 'success'}
		try:
			best = self.recipe_db.get_highest_rated_by_category(category.lower()) #get the highest rated 
			output['title'] = best.title
			output['rec_id'] = best.rec_id
			output['rating'] = best.rating
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		return json.dumps(output)


	#post will either return a matching difficulty recipe for a fiven category or healthiest per category depending on the contents of the body


	def POST(self, category):
		output = {'result' : 'success'}
		the_body = cherrypy.request.body.read(int(cherrypy.request.headers['Content-Length']))
		try:
			the_body = json.loads(the_body)
			difficulty = the_body['difficulty']
			calories = int(the_body['calories'])
			the_list = self.recipe_db.get_difficult_categories(category, difficulty)	
			the_new_list = self.recipe_db.get_healthiest_with_filter(the_list, calories)
				
			best = {}
			for el in the_new_list:
				best[el.rec_id] = el.title
			
			
			output['diff/cat'] = best 

		except KeyError as ey:
			try:
				ingredient_list = the_body['healthiest']
				best = self.recipe_db.get_healthiest_by_category(category.lower())
				
				output['title'] = best.title
				output['rec_id'] = best.rec_id
				output['rating'] = best.rating
				output['protein'] = best.protein
				output['fat'] = best.fat
				output['sodium'] = best.sodium
				output['calories'] = best.calories

			except KeyError as ez:
				output['result'] = 'error'
				output['message'] = 'key not found'

		return json.dumps(output)
