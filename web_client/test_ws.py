import unittest
import requests
import json

class TestRecipes(unittest.TestCase):
  PORT_NUM = '51028'
  print("Testing Port number: ", PORT_NUM)
  SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM
  RECIPE_URL = SITE_URL +'/recipes/'
  RESET_URL = SITE_URL + '/reset/'
  CATEGORIES_URL = SITE_URL + '/categories/'
  DIFFICULTY_URL = SITE_URL + '/difficulty/'
 
  def reset_data(self):
    m = {}
    r = requests.put(self.RESET_URL, data = json.dumps(m))

  def is_json(self, resp):
    try:
      json.loads(resp)
      return True
    except ValueError:
      return False

  def test_recipes_get(self):
    self.reset_data()
    r = requests.get(self.RECIPE_URL)
    self.assertTrue(self.is_json(r.content.decode()))
    resp = json.loads(r.content.decode())
    recipes = resp['titles']


    #for rec in recipes:
    #  if rec['rec_id'] == 40:
    #    tectrec = rec
    self.assertEqual(recipes[39], 'Better-Than-Pita Grill Bread ')
   # self.assertEqual(testmovie['calories'], 145.0)

  def test_recipes_get_key(self):
    self.reset_data()
    rec_id = 40
    r = requests.get(self.RECIPE_URL + str(rec_id))
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['title'], 'Better-Than-Pita Grill Bread ')
    self.assertEqual(resp['calories'], 145.0)

  def test_recipes_delete(self):
    self.reset_data()
    rec_id = 40

    m = {}
    r = requests.delete(self.RECIPE_URL + str(rec_id), data = json.dumps(m))
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['result'], 'success')

    r = requests.get(self.RECIPE_URL + str(rec_id))
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['result'], 'error')

  def test_recipes_post(self):
    self.reset_data()
    m = {'title': "Test", 'category': ["Test"], 'directions': ["Just a test"], 'rating': 5.0, 'ingredients':["test", "test", "test"], 'calories': 100, 'protein': 20, 'fat':10, 'sodium':10}
    r = requests.post((self.RECIPE_URL), data = json.dumps(m))
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    x = resp["rec_id"]

    r = requests.get(self.RECIPE_URL + str(x))
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['title'], 'Test')
    
    m = {'ingredients': ["test"]}
    r = requests.post((self.RECIPE_URL), data = json.dumps(m))
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['title'], 'Test')
    
    m = {'bogus': ["test"]}
    r = requests.post((self.RECIPE_URL), data = json.dumps(m))
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['result'], 'error')

  def test_get_difficulty(self):
    #m = {'title': "Test", 'category': ["Test"], 'directions': ["Just a test"], 'rating': 5.0, 'ingredients':["test", "test", "test"], 'calories': 100, 'protein': 20, 'fat':10, '
   self.reset_data()
   r = requests.get(self.DIFFICULTY_URL + 'easy')
   self.assertTrue(self.is_json(r.content.decode('utf-8')))
   resp = json.loads(r.content.decode('utf-8'))
   self.assertEqual(resp['title'], 'Mozzarella-Topped Peppers with Tomatoes and Garlic ')

  def test_get_category(self):
    self.reset_data()
    r = requests.get(self.CATEGORIES_URL)
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['Categories'][2], 'fruit')

  def test_get_highest_rated_in_category(self):
    self.reset_data()
    
    m = {'title': "Test", 'category': ["Test"], 'directions': ["Just a test"], 'rating': 5.0, 'ingredients':["test", "test", "test"], 'calories': 100, 'protein': 20, 'fat':10, 'sodium':10}
    r = requests.post((self.RECIPE_URL), data = json.dumps(m)) 
    r = requests.get(self.CATEGORIES_URL + 'test')
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['title'], 'Test')
    self.assertEqual(resp['rating'], 5.0)

  def test_category_post(self):
    self.reset_data()
    #m = {"difficulty" : "easy"}
    #r = requests.post(self.CATEGORIES_URL + 'test')
    #m = {'title': "Test", 'category': ["Test"], 'directions': ["Just a test"], 'rating': 4.0, 'ingredients':["test", "test", "test"], 'calories': 100, 'protein': 20, 'fat':10, 'sodium':10}
    #r = requests.post((self.RECIPE_URL), data = json.dumps(m))
    #m = {'title': "Test2", 'category': ["Test"], 'directions': ["Just a test"], 'rating': 5.0, 'ingredients':["test", "test", "test"], 'calories': 100, 'protein': 20, 'fat':10, 'sodium':10}         
    #r = requests.post((self.RECIPE_URL), data = json.dumps(m))
    m = {'difficulty' : "easy"}
    r = requests.post(self.CATEGORIES_URL + 'chicken', json.dumps(m))
    self.assertTrue(self.is_json(r.content.decode('utf-8')))
    resp = json.loads(r.content.decode('utf-8'))
    self.assertEqual(resp['title'], 'Leftover-Roast-Chicken-Stock ')
    #m = {'title': "Test", 'category': ["chicken"], 'directions': ["Just a test"], 'rating': 6.0, 'ingredients':["test", "test", "test"], 'calories': 100, 'protein': 20, 'fat':10, 'sodium':10}       
    #r = requests.post((self.RECIPE_URL), data = json.dumps(m))
    #self.assertTrue(self.is_json(r.content.decode('utf-8')))
    #self.assertEqual(resp['title'], 'Test')
if __name__ == "__main__":
  unittest.main()


