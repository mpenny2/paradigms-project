from _recipe_database import _recipe_database
import json
import re
import cherrypy

class DifficultyController(object):
	def __init__(self, rdb = None):
		if rdb == None:
			self.recipe_db = _recipe_database()
		else:
			self.recipe_db = rdb

	#will return the highest rated for a given difficulty
	

	def GET(self, level):
		output = {'result' : 'success'}
		try:
			best = self.recipe_db.get_best_desired_difficulty(level)
			output['title'] = best.title
			output['rec_id'] = best.rec_id
			output['rating'] = best.rating
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		return json.dumps(output)
    #
