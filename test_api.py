#!/usr/bin/env python

from _recipe_database import _recipe_database
import unittest

class TestRecipeDatabase(unittest.TestCase):
        """unit tests for python primer homework"""

        #@classmethod
        #def setUpClass(self):
        recipe_db = _recipe_database()

        def reset_data(self):
                self.recipe_db.delete_all_recipes()
                "reset data is required because we cannot promise an order of test case execution"
                self.recipe_db.load_recipes('data/full_format_recipes.json')

        def test_load_recipes(self):
            self.reset_data()
            self.assertEquals(len(self.recipe_db.recipes), 19551)
                #self.assertEquals(movie[1], 'Adventure|Children\'s|Fantasy')

        def test_add__and_delete_recipe(self):
            self.reset_data()
            self.recipe_db.add_recipe("Test Recipe", ["Python"], "This is merely a test", 6.7, ["Linux", "Bash", "Ubuntu"], 700)
            #print(len(self.recipe_db.recipes))

            self.assertEquals(len(self.recipe_db.recipes), 19552)
            self.recipe_db.delete_recipe(19552)
            # self.assertEquals(len(self.recipe_db.recipes), 19551)


        def test_get_by_category(self):
            self.reset_data()
            self.recipe_db.add_recipe("Test Recipe", ["Python"], "This is merely a test", 6.7, ["Linux", "Bash", "Ubuntu"], 700)
            tempList = self.recipe_db.get_by_category("python")
            self.assertEquals(len(tempList), 1)
            self.assertEquals(tempList[0].title, "Test Recipe")
            self.recipe_db.add_recipe("Test Recipe 2", ["Python"], "This is merely a test", 6.7, ["Linux", "Bash", "Ubuntu"], 700)
            tempList = self.recipe_db.get_by_category("python")
            self.assertEquals(len(tempList), 2)
            self.assertEquals(tempList[1].title, "Test Recipe 2")

        def test_get_highest_rated_by_category(self):
            self.reset_data()
            self.recipe_db.add_recipe("Test Recipe 1", ["Python"], "This is merely a test", 10, ["Linux", "Bash", "Ubuntu"], 700)
            self.recipe_db.add_recipe("Test Recipe 2", ["Python"], "This is merely a test", 9, ["Linux", "Bash", "Ubuntu"], 700)
            self.recipe_db.add_recipe("Test Recipe 3", ["Python"], "This is merely a test", 8, ["Linux", "Bash", "Ubuntu"], 700)
            temp = self.recipe_db.get_highest_rated_by_category("Python")
            self.assertEquals(temp.rating, 10)
            self.assertEquals(temp.title, "Test Recipe 1")
            self.recipe_db.delete_recipe(19552)
            temp = self.recipe_db.get_highest_rated_by_category("Python")
            self.assertEquals(temp.rating, 9)
            self.assertEquals(temp.title, "Test Recipe 2")

        def test_most_ingredients(self):
            self.reset_data()
            self.recipe_db.add_recipe("Test Recipe 1", ["Python"], "This is merely a test", 10, ["Linux", "Bash", "Ubuntu", "Shell"], 700)
            self.recipe_db.add_recipe("Test Recipe 2", ["Python"], "This is merely a test", 10, ["Stack", "Buffer", "Malloc"], 700)
            temp = self.recipe_db.get_most_ingredients(["Linux"])
            self.assertEquals(temp.title, "Test Recipe 1")
            temp = self.recipe_db.get_most_ingredients(["Malloc"])
            self.assertEquals(temp.title, "Test Recipe 2")
            temp = self.recipe_db.get_most_ingredients(["Chicken"])
            self.assertEquals(temp.title, "Chicken Soup ")


        def test_get_healthiest_by_category(self):
            self.reset_data()
            healthiest = self.recipe_db.get_healthiest_by_category("Chicken")
            self.assertEquals(healthiest.title, 'Achiote Chicken with Tangerine Sauce ')
            self.assertEquals(healthiest.protein, 54.0)

        def test_get_best_desired_difficulty(self):
            self.reset_data()
            easiest = self.recipe_db.get_best_desired_difficulty("easy")
            medium = self.recipe_db.get_best_desired_difficulty("medium")
            hardest = self.recipe_db.get_best_desired_difficulty("hard")
            self.assertEquals(easiest.title, 'Mozzarella-Topped Peppers with Tomatoes and Garlic ')
            self.assertEquals(medium.title, 'Mahi-Mahi in Tomato Olive Sauce ')
            self.assertEquals(hardest.title, 'Tuna, Asparagus, and New Potato Salad with Chive Vinaigrette and Fried Capers ')

        def test_get_difficulty_with_category(self):
            self.reset_data()
            accurate = self.recipe_db.get_difficulty_with_category("Chicken", "easy")
            self.assertEquals(accurate, 'Leftover-Roast-Chicken-Stock ')

        def test_get_by_difficulty(self):
            self.reset_data()
            difficult_list = self.recipe_db.get_by_difficulty("hard")
            self.assertEquals(len(difficult_list), 6652)

if __name__ == "__main__":
    unittest.main()
