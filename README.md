Our API is centered around provising useful information and manipulating a database of recipes that 
has been constructed by reading through a json data source containing thousands of recipes.

From a base level, our API is able to load in all of the recipes from the json into a native database, 
from which more complex functions can be performed on it.  Through our API, we are able to retrieve one 
recipe by passing its recipe_id as an argument to a function, as well as retrieve a 
list of recipes that share a common category that is passed as an argument.

It is also is able to add recipes to the database, get the highest rated recipe from a given category, 
return the recipe with the most ingredient matches to a user input, return a fiiting recipe for an
inputted category and difficulty level, return all recipes for a given difficulty level, return the 
healthiest option in a given category in terms of fat, protein, and sodium, and delete specific recipes as 
well as all of the recipes.

The flexibility and breadth of our API will allow for our front-end web application to serve many different
requests and user behaviors.

